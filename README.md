# ArduRobot

Библиотеки и скетчи Arduino

## Лицензии
* Скетчи:
  + **Motor.ino** | [MIT License](LICENSE) | **Старинин Андрей (AnSt), 2018**
  + **ShortTrack.ino** | [MIT License](LICENSE) | **Старинин Андрей (AnSt), 2018**
  + **Labirint.ino** | [MIT License](LICENSE) | **Старинин Андрей (AnSt), 2018**
* Библиотеки:
  + **AFMotor** | *Информации о лицензии нет*
  + **Ultrasonic** | [MIT License](https://github.com/ErickSimoes/Ultrasonic/blob/master/LICENSE) | **Copyright &copy; 2017 Erick Simões**

## Состав
* **Library** - набор необходимых бибилиотек.
* **Motor** - набор функций для управления платформой с двумя двигателями. *Arduino + Adafruit motor shield*
* **ShortTrack** - готовый скетч для соревнования Шорт-Трек. *Arduino + Adafruit motor shield + HC-SR04*
* **Labirint** - готовый скетч для соревнования Лабиринт. *Arduino + Adafruit motor shield + HC-SR04*

## Подробное описание
* [Motor.md](Motor/Motor.md)
* [ShortTrack.md](ShortTrack/ShortTrack.md)
* [Labirint.md](Labirint/Labirint.md)

## Аппаратная часть
* Adafruit motor shield
	+ [Adafruit motor shield](https://www.adafruit.com/product/81#Learn).
	+ [Adafruit motor shield](https://learn.adafruit.com/adafruit-motor-shield)
	+ [Adafruit-Motor-Shield-library](https://github.com/adafruit/Adafruit-Motor-Shield-library)
	+ [Описание на русском](http://zelectro.cc/Adafruit_motor_shield)
* HC-SR04
	+ [Ultrasonic](https://github.com/ErickSimoes/Ultrasonic) 